import json
import ntpath
import torch
import torch.nn as nn
from torchvision import transforms, datasets
from torch.utils.data import DataLoader
import sys
  
# класс модели
class FaceClassifier(nn.Module):
    def __init__(self):
        super().__init__()
        self.block1 = self.conv_block(c_in=3, c_out=128, dropout=0.1, kernel_size=5, stride=1, padding=2)
        self.block2 = self.conv_block(c_in=128, c_out=64, dropout=0.1, kernel_size=3, stride=1, padding=1)
        self.block3 = self.conv_block(c_in=64, c_out=32, dropout=0.1, kernel_size=3, stride=1, padding=1)
        self.lastcnn = nn.Conv2d(in_channels=32, out_channels=2, kernel_size=16, stride=1, padding=0)
        self.maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
    def forward(self, x):
        x = self.block1(x)
        x = self.maxpool(x)
        x = self.block2(x)
        x = self.block3(x)
        x = self.maxpool(x)
        x = self.lastcnn(x)
        return x
    def conv_block(self, c_in, c_out, dropout,  **kwargs):
        seq_block = nn.Sequential(
            nn.Conv2d(in_channels=c_in, out_channels=c_out, **kwargs),
            nn.BatchNorm2d(num_features=c_out),
            nn.ReLU(),
            nn.Dropout2d(p=dropout)
        )
        return seq_block
    
# функция для того, чтобы выделить из имени файла с картинкой название картинки без папок
def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)

def main(path):
    '''
    Процедура обработки переданной папки с изображениями.
    
    Параметры:
    path - путь к папке, указывается только название папки в виде строки, например "processing". 
    
    Результат выполнения:
    Записывает в ту папку, из которой вызывался скрипт, файл "processing.json" с результатами предсказаний классов картинок.
    Пример вывода:  { ‘img_1.jpg’: ‘male’, ‘img_2.jpg’: ‘female’, ...}
    
    Примечание:
    Из-за особенностей процессинга изображений в pytorch, в переданной папке должна находиться папка, а уже в ней изображения
    '''
    
    # загрузка настроенной модели
    print('Загрузка модели')
    net = FaceClassifier()
    net.load_state_dict(torch.load('./face_classification_model.pth'))
    net.eval()
    
    # загрузка датасета из переданной папки с картинками
    print('Подготовка данных')
    image_transforms = {
    "data": transforms.Compose([
        transforms.Resize((64, 64)), # приведение изображения к размеру 64х64
        transforms.ToTensor() # приведение изоражения к тензору, который модель будет обрабатывать
    ])
    }
    face_dataset = datasets.ImageFolder(root = path,
                                      transform = image_transforms["data"]
                                     )
    face_loader = DataLoader(dataset=face_dataset, shuffle=False, batch_size=1)
    
    # предсказание классов (0 - женщина, 1 - мужчина)
    print('Обработка данных')
    y_pred_list = []
    image_list = []
    with torch.no_grad():
        for x_batch, _ in face_loader:
            y_test_pred = net(x_batch)
            y_test_pred = torch.log_softmax(y_test_pred, dim=1)
            _, y_pred_tag = torch.max(y_test_pred, dim = 1)
            y_pred_list.append(y_pred_tag.cpu().numpy())
    
    # формирование списка файлов с предсказаниями
    print('Сохранение результата обработки')
    output_list = []
    class_labels = {0:'female', 1:'male'}
    for y, img in zip(y_pred_list, face_dataset.imgs):
        img_name = path_leaf(img[0])
        y_label = class_labels[int(y[0][0][0])]
        output_list.append({img_name: y_label})
    
    # сохранение в файл json
    with open('processing.json', 'w') as file:
        json.dump(output_list, file)
        
    print('Обработка завершена. Результат обработки см. в файле processing.json')
    
if __name__ == "__main__":
    main(sys.argv[1])
  